var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/index_list/data', function(req, res, next){
	res.send({
		"code":0,
		"data":{
			topBar:[
				{id:1,name:"推荐"},
				{id:2,name:"运动户外"},
				{id:3,name:"服饰内衣"},
				{id:4,name:"鞋靴箱包"},
				{id:5,name:"美妆护肤"},
				{id:6,name:"家居数码"},
				{id:7,name:"食品母婴"}
			],
			data:[
				{
					type:"swiperList",
					data:[
						{imgUrl:'../../static/imgs/实时会记.jpeg'},
						{imgUrl:'../../static/imgs/精准税务.jpeg'},
						{imgUrl:'../../static/imgs/精准税务.jpeg'}
					]
				},
				{
					type:"recommendList",
					data:[
						{
							begUrl:'@/static/imgs/实时会记.jpeg',
							data:[
								{imgUrl:'../../static/imgs/实时会记.jpeg'},
								{imgUrl:'../../static/imgs/精准税务.jpeg'},
								{imgUrl:'../../static/imgs/精准税务.jpeg'},
							]
						},
						{
							begUrl:'@/static/imgs/实时会记.jpeg',
							data:[
								{imgUrl:'../../static/imgs/实时会记.jpeg'},
								{imgUrl:'../../static/imgs/精准税务.jpeg'},
								{imgUrl:'../../static/imgs/精准税务.jpeg'},
							]
						}
					]
				},
				{
					type:"commoditylist",
					data:[
						{
							id:1,
							imgUrl:"../../static/imgs/实时会记.jpeg",
							name:"大衣绒毛大款2020年必须买，不买你就不行了，爆款疯狂GG008",
							pprice:"299",
							oprice:"659",
							discount:"5.2",
						},{
							id:2,
							imgUrl:"../../static/imgs/精准税务.jpeg",
							name:"大衣绒毛大款2020年必须买，不买你就不行了，爆款疯狂GG008",
							pprice:"299",
							oprice:"659",
							discount:"5.2",
						},{
							id:3,
							imgUrl:"../../static/imgs/敏捷财资.jpeg",
							name:"大衣绒毛大款2020年必须买，不买你就不行了，爆款疯狂GG008",
							pprice:"299",
							oprice:"659",
							discount:"5.2",
						},{
							id:4,
							imgUrl:"../../static/imgs/智能财务.jpeg",
							name:"大衣绒毛大款2020年必须买，不买你就不行了，爆款疯狂GG008",
							pprice:"299",
							oprice:"659",
							discount:"5.2",
						},{
							id:5,
							imgUrl:"../../static/imgs/实时会记.jpeg",
							name:"大衣绒毛大款2020年必须买，不买你就不行了，爆款疯狂GG008",
							pprice:"299",
							oprice:"659",
							discount:"5.2",
						},{
							id:6,
							imgUrl:"../../static/imgs/智能财务.jpeg",
							name:"大衣绒毛大款2020年必须买，不买你就不行了，爆款疯狂GG008",
							pprice:"299",
							oprice:"659",
							discount:"5.2",
						}
					]
				}
			]
		}
	});
});


module.exports = router;
